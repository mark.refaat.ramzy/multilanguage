<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert(array(
            array(
                'code' => 'en',
                'name' => 'english',
            ),
            array(
                'code' => 'ar',
                'name' => 'arabic',
            ),
        ));
        DB::table('products')->insert(array(
            array(
                'price' => '10',
            ),
            array(
                'price' => '22.5',
            ),
            array(
                'price' => '30',
            ),
        ));
        DB::table('product_translations')->insert(array(
            array(
                'product_id' => '1',
                'language_code' => 'en',
                'title' => 'milk',
                'description' => 'fresh milk'
            ),
            array(
                'product_id' => '2',
                'language_code' => 'en',
                'title' => 'cheese',
                'description' => 'sheeder cheese'
            ),
            array(
                'product_id' => '3',
                'language_code' => 'en',
                'title' => 'chicken',
                'description' => '1 chicken'
            ),
            array(
                'product_id' => '1',
                'language_code' => 'ar',
                'title' => 'لبن',
                'description' => 'لبن فريش'
            ),
            array(
                'product_id' => '2',
                'language_code' => 'ar',
                'title' => 'جبنة',
                'description' => 'جبنة شيدر'
            ),
            // array(
            //     'product_id' => '3',
            //     'language_code' => 'ar',
            //     'title' => 'فراخ',
            //     'description' => 'فرخة واحدة'
            // ),
        ));
        // $this->call(UserSeeder::class);
    }
}
