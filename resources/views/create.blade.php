@extends('layouts.app')
@section('content')
<div class="container">
    <form method="POST" action="{{ route('products.store', [App::getLocale()]) }}">
        @csrf
        <div class="form-group">
            <label for="title">{{ __('products_table.title') }}</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <br>
        <div class="form-group">
            <label for="description">{{ __('products_table.desc') }}</label>
            <input type="text" class="form-control" id="description" name="description">
        </div>
        <br>
        <div class="form-group">
            <label for="price">{{ __('products_table.price') }}</label>
            <input type="number" class="form-control" id="price" name="price">
        </div>
        <br>
        <button type="submit" class="btn btn-primary">{{ __('products_table.add') }}</button>
    </form>
</div>
@endsection