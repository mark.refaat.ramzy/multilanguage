@extends('layouts.app')
@section('content')
<style>
    .tableformat td:nth-child(1) {
        width: 700px;
    }
</style>
<div class="container">
    <div class="justify-content-md-center">
        <div class="text-center">
            <span style="font-size: 25px; font-weight: bold"> Basket</span>
            <hr width="30px" class="mb-5">
        </div>
        @if (session()->has('cart'))
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">PRODUCT</th>
                                <th scope="col">PRICE</th>
                                <th scope="col">QUANTITY</th>
                                <th scope="col">SUBTOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (Session::get('cart') as $item)
                            <tr class="tableformat">
                                <td class="d-none">{{ $item[0] }}</td>
                                <td scope="row">{{ $item[1]}}</td>
                                <td>${{ $item[2] }}</td>
                                <td>
                                    <div class="row">
                                        <input type="button" class="col px-0 ml-2 text-center" value="-">
                                        <div class="col px-1 text-center pt-1">1</div>
                                        <input type="button" class="col px-0 mr-5 text-center" value="+">
                                    </div>
                                </td>
                                <td>${{ $item[2] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="mx-2 d-none d-md-block d-lg-block" style="border-right: 1px solid grey; height: 500px">
                </div>
                <div class="col">
                    BASKET TOTALS
                    <hr>
                    <div class="row px-3">
                        Subtotal <span class="ml-auto">$65.00</span>
                    </div>
                    <hr>
                    <div class="row px-3">
                        Shipping
                        <div class="col px-0 ml-auto text-right">
                            Flat rate: $5.00
                            <div style="font-size: 12px;">
                                Shipping options will be updated during checkout.
                            </div>
                            Calculate shipping
                        </div>
                    </div>
                    <hr>
                    <div class="row px-3">
                        Total <span class="ml-auto">$70.00</span>
                    </div>

                    <hr>
                    <a href="{{ route('checkout', App::getLocale()) }}" class="btn btn-warning text-white"
                        style="width: 100%">Proceed to
                        checkout</a>
                    <div class="text-center py-2">— OR —</div>
                    <button class="btn btn-warning text-white" style="width: 100%">Paypal</button>
                    <div class="pb-2 pt-4"><i class="fas fa-tag"></i> Coupon</div>
                    <input class="input-group-text mb-3" style="display: block; width:100%" type="text"
                        placeholder="Coupon Code">
                    <button class="btn btn-outline-secondary" style="width: 100%;">Apply Coupon</button>
                </div>
            </div>
        </div>
        @else
        <div class="text-center">
            <div class="mb-3">
                Your basket is currently empty.
            </div>
            <a href="{{ route('products.index', App::getLocale()) }}" class="btn bg-dark text-white">RETURN TO SHOP</a>
        </div>
        @endif

    </div>
</div>
@endsection