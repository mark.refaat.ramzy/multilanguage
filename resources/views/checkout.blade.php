@extends('layouts.app')
@section('content')
@php
$total =0;
@endphp
<div class="container">
    <div class="justify-content-md-center">
        <div class="text-center">
            <span style="font-size: 25px; font-weight: bold">
                <a href="" class="grey_no_underline">SHOPPING CART</a>
                <i class="fas fa-chevron-right grey_no_underline"></i>
                <a href="" class="grey_no_underline" style="color: black">CHECKOUT DETAILS</a>
                <i class="fas fa-chevron-right grey_no_underline"></i>
                <a href="" class="grey_no_underline ">ORDER COMPLETE</a>
            </span>
            <hr width="30px" class="mb-5">
        </div>

        {{-- <div class="mb-3">
            Your basket is currently empty.
        </div> 
        <a href="" class="btn bg-dark text-white">RETURN TO SHOP</a> --}}
        <div class="container">

            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-12">
                    Returning customer? <a href="">Click here to login</a>
                    <br>
                    Have a coupon? <a href=""> Click here to enter your code</a>
                    <hr>
                    BILLING DETAILS
                    <div class="row">
                        <div class="col">
                            <div class="py-2">First Name <span style="color: red">*</span></div>
                            <input class="input-group-text" style="width: 100%">
                        </div>
                        <div class=" col">
                            <div class="py-2">Last Name <span style="color: red">*</span></div>
                            <input class="input-group-text" style="width: 100%">
                        </div>
                    </div>
                    <div class="py-2">Company name (optional)</div>
                    <input class="input-group-text" style="width: 100%">
                    <div class="py-2">Country / Region <span style="color: red">*</span></div>
                    <input class="input-group-text" style="width: 100%">
                    <div class="py-2">Street address <span style="color: red">*</span></div>
                    <input class="input-group-text" style="width: 100%">
                    <div class="py-2">Town / City <span style="color: red">*</span></div>
                    <input class="input-group-text" style="width: 100%">
                    <div class="py-2">State / County <span style="color: red">*</span></div>
                    <input class="input-group-text" style="width: 100%">
                    <div class="py-2">Postcode / ZIP <span style="color: red">*</span></div>
                    <input class="input-group-text" style="width: 100%">
                    <div class="py-2">Phone <span style="color: red">*</span></div>
                    <input class="input-group-text" style="width: 100%">
                    <div class="py-2">Email address <span style="color: red">*</span></div>
                    <input class="input-group-text" style="width: 100%">
                    <div class="py-2">
                        <input type="checkbox"> Create an account?
                    </div>
                    <div class="py-2">
                        <input type="checkbox"> Deliver to a different address?
                    </div>
                    Order notes (optional)
                    <textarea name="" id="" rows="5" style="width:100%"
                        placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                </div>
                <div class="col border py-3" style="height: 770px">
                    YOUR ORDER

                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">PRODUCT</th>
                                <th scope="col">SUBTOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (Session::get('cart') as $item)
                            <tr>
                                <td scope="row">{{ $item[1] }} × 1</td>
                                <td>${{ $item[2] }}</td>
                                @php
                                $total += $item[2];
                                @endphp
                            </tr>
                            @endforeach
                            <tr>
                                <td scope="row">Subtotal</td>
                                <td>${{ $total }}</td>
                            </tr>
                            <tr>
                                <td scope="row">Shipping</td>
                                <td>Flat rate: $5.00</td>
                            </tr>
                            <tr>
                                <td scope="row">Total</td>
                                <td>${{ $total + 5 }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <input type="radio"> Cash on delivery
                    <br>
                    Pay with cash upon delivery.
                    <hr>
                    <input type="radio"> Credit Card
                    <hr>
                    <input type="radio"> PayPal
                    <hr>
                    <input type="checkbox"> I have read and agree to the website terms and conditions *
                    <br>
                    <input type="button" value="PLACE ORDER" class="btn btn-warning text-white mt-2">
                    <div class="py-2">
                        Your personal data will be used to process your order, support your experience throughout this
                        website, and for other purposes described in our privacy policy.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection