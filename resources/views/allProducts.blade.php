@extends('layouts.app')
@section('content')


<div class="container">
    <a href="{{ route('products.create',  App::getLocale() ) }}"
        class="btn btn-primary">{{ __('products_table.add') }}</a>
    <table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>{{ __('products_table.title') }}</th>
                <th>{{ __('products_table.desc') }}</th>
                <th>{{ __('products_table.price') }}</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <td class="id">{{ $product->id }}</td>
                <td class="name">{{ $product->translation[0]->title }}</td>
                <td>{{ $product->translation[0]->description }}</td>
                <td class="price">{{ $product->price }}</td>
                <td>
                    <a href='{{ route('products.edit', [App::getLocale(), $product->id]) }}'
                        class='btn btn-secondary'>{{ __('products_table.edit') }}</a>
                    <form action='{{ route('products.destroy', [App::getLocale() , $product->id]) }}' method='POST'
                        style='display:inline'>
                        @csrf
                        @method('delete')
                        <input onclick='return confirm("{{ __("home.delete_product_confirm_message") }}");'
                            type='submit' value=' {{ __('products_table.delete') }}' class='btn btn-danger'>
                    </form>
                    <button class="btn btn-primary add_to_cart">Add to cart</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    $(document).ready( function () {
        $('#datatable1').DataTable();
    });

    $('.add_to_cart').on('click', function(){
        var id = $(this).closest("tr").find('.id').text();
        var name = $(this).closest("tr").find('.name').text();
        var price = $(this).closest("tr").find('.price').text();
        window.location.href = "addToCart/" + id + "/" + name + "/" + price;
        // $("#totalBasket").text(parseFloat($("#totalBasket").text()) + parseFloat(price));
    });

</script>
@endsection