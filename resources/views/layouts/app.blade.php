{{ Session::get('totalBasket') ? "t" : "n" }}

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{ config('app.name', 'MultiLang') }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
        integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
        crossorigin="anonymous" />
    <style>
        .grey_no_underline {
            color: gray;
            text-decoration: none;
        }

        .fa-stack:hover {
            color: #000000;
        }

        .dropdown:hover .dropdown-menu {
            display: block;
            margin-top: 0; // remove the gap so it doesn't close
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js">
    </script>
</head>

<body dir="{{ __('home.dir') }}">
    {{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/'. App::getLocale()) }}"
    style="color:#45b3e7; text-transform:uppercase; font-weight:bold;">
    {{__("home.app_name") }}
    </a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link"
                    href="{{ route('products.index', App::getLocale()) }}">{{ __('home.navbar_products_link') }}</a>
            </li>
            <li class="nav-item">

                <a class="nav-link"
                    href="{{ preg_replace(App::getLocale() == 'en' ? '/en\b/i' : '/ar\b/i', App::getLocale() == 'en' ? 'ar' : 'en' ,url()->current()) }}">
                    {{ App::getLocale() == 'en' ? 'عربي' : 'english' }}
                </a>
            </li>
        </ul>

    </div>
    </div>
    </nav> --}}

    <nav class="navbar sticky-top navbar-light bg-light p-0">
        <div class="col p-0">
            <div class="bg-dark text-white text-center mb-2">
                <span style="font-size: 13px;">COVID-19: We ask for your understanding as you may experience delivery
                    delays</span>
                <span class="pl-5"><i class="fas fa-envelope-square"></i> NEWSLETTER</span>
                <br>
                <br>
                <a class="text-white" href="{{ route('products.index', App::getLocale()) }}">ALL PRODUCTS</a>
            </div>
            <div class="row text-center">
                <div class="col pt-4">
                    <div class="row justify-content-md-center d-none d-md-flex d-lg-flex">
                        <a href="#" class="rounded-circle grey_no_underline">
                            <span class="fa-stack" style="vertical-align: top;">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-search fa-stack-1x"></i>
                            </span>
                        </a>
                        <div class="mx-2" style="border-right: 1px solid grey; height: 35px"></div>
                        <a href="#" class="rounded-circle grey_no_underline">
                            <span class="fa-stack" style="vertical-align: top;">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-user-alt fa-stack-1x"></i>
                            </span>
                        </a>
                        <div class="mx-2" style="border-right: 1px solid grey; height: 35px"></div>
                        <a href="#" class="rounded-circle grey_no_underline">
                            <span class="fa-stack" style="vertical-align: top;">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </span>
                        </a>
                    </div>
                    {{-- SMALL SCREEN --}}
                    <div class="row justify-content-md-center ml-3 d-none d-sm-flex d-md-none d-lg-none">
                        <a href="#" class="rounded-circle grey_no_underline">
                            <span class="fa-stack" style="vertical-align: top;">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-bars fa-stack-1x"></i>
                            </span>
                        </a>
                        <div class="mx-2" style="border-right: 1px solid grey; height: 35px"></div>
                        <a href="#" class="rounded-circle grey_no_underline">
                            <span class="fa-stack" style="vertical-align: top;">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-shopping-bag fa-stack-1x"></i>
                            </span>
                        </a>
                    </div>
                    {{-- END SMALL SCREEN --}}
                </div>
                <div class="col-5">
                    <img src="{{ asset('logo.jpg') }}" alt="Logo Image" width="160" height="100" class="rounded">
                </div>
                <div class="col">
                    <div class="row d-none d-md-flex d-lg-flex">
                        <a href="{{ route('basket', App::getLocale()) }}" class="px-3 py-1 mt-4 mr-4"
                            style="border-radius: 30px; background-color: black; color: white;">
                            Basket / $ <span
                                id="totalBasket">{{ Session::get('totalBasket') ? Session::get('totalBasket') : '0.00' }}</span>
                            <i class="fas fa-shopping-bag"></i>
                        </a>
                        <div class="mt-4" style="border-right: 1px solid grey; height: 35px"></div>
                        <div class="px-3 py-1 mt-4 ml-4"
                            style="border-radius: 30px; background-color: #f6c217; color: white;">
                            Checkout
                        </div>

                    </div>
                    <div class="row mt-4 d-none d-sm-flex d-md-none d-lg-none">
                        <a href="#" class="rounded-circle grey_no_underline">
                            <span class="fa-stack" style="vertical-align: top;">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </span>
                        </a>
                        <div class="mx-2" style="border-right: 1px solid grey; height: 35px"></div>
                        <a href="#" class="rounded-circle grey_no_underline">
                            <span class="fa-stack" style="vertical-align: top;">
                                <i class="far fa-circle fa-stack-2x"></i>
                                <i class="fas fa-user-alt fa-stack-1x"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <hr class="mx-5">

            <div class="row text-center mb-2">
                <div class="col-md-12 text-center">
                    <div class="dropdown btn-group">
                        <a style="text-transform: uppercase; background-color: Transparent; border: none;"
                            class="dropdown-toggle" type="button" data-toggle="dropdown">Fashion
                            <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu p-4">
                            <a class="grey_no_underline" href="#">TROUSERS</a>

                            <a class="grey_no_underline" href="#">T-SHIRT</a>
                            <hr class="my-2">
                            <a class="grey_no_underline" href="#">SHIRTS AND BLOUSES</a>
                            <hr class="my-2">
                            <a class="grey_no_underline" href="#">DRESSES</a>
                            <hr class="my-2">
                            <a class="grey_no_underline" href="#">SKIRTS</a>
                            <hr class="my-2">
                            <a class="grey_no_underline" href="#">JACKETS</a>
                            <hr class="my-2">
                            <a class="grey_no_underline" href="#">SHORTS</a>
                            <hr class="my-2">
                            <a class="grey_no_underline" href="#">JUMPSUITS</a>
                            <hr class="my-2">
                            <a class="grey_no_underline" href="#">SETS</a>
                        </div>
                    </div>
                    <div class="dropdown btn-group mx-3">
                        <a style="text-transform: uppercase; background-color: Transparent; border: none;"
                            class="dropdown-toggle" type="button" data-toggle="dropdown">Lingerie
                            <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu p-4" style="width: 350px">
                            <div class="row">
                                <div class="col">
                                    <a class="grey_no_underline" href="#">BRA</a>
                                    <hr class="my-2">
                                    <a class="grey_no_underline" href="#">Unlined</a>
                                    <hr class="my-2">
                                    <a class="grey_no_underline" href="#">Lightly Lined</a>
                                    <hr class="my-2">
                                    <a class="grey_no_underline" href="#">Double Push Up</a>
                                    <hr class="my-2">
                                    <a class="grey_no_underline" href="#">Strapless</a>
                                    <hr class="my-2">
                                    <a class="grey_no_underline" href="#">Sets</a>
                                </div>
                                <div class="col">
                                    <a class="grey_no_underline" href="#">PANTIES</a>
                                    <hr class="my-2">
                                    <a class="grey_no_underline" href="#">V/G String</a>
                                    <hr class="my-2">
                                    <a class="grey_no_underline" href="#">Brazilian</a>
                                    <hr class="my-2">
                                    <a class="grey_no_underline" href="#">Briefs</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a style="text-transform: uppercase; background-color: Transparent; border: none;"
                        type="button">Jewlery
                    </a>
                </div>
            </div>

        </div>
        {{-- <a class="navbar-brand" href="#">Navbar</a>
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div> --}}
    </nav>

    <main class="py-4">
        @yield('content')
    </main>

    <footer class="text-white bg-dark">
        <div class="container pt-5">
            <ul style="list-style-type: none;">
                <li class="mb-3"><a style="color: white; text-decoration: none;" href="#">Contact Us</a></li>
                <li class="mb-3"><a style="color: white; text-decoration: none;" href="#">Privacy Policy</a></li>
                <li class="mb-3"><a style="color: white; text-decoration: none;" href="#">Terms & Conditions</a></li>
                <li class="mb-3"><a style="color: white; text-decoration: none;" href="#">Delivery Policy</a></li>
                <li class="mb-3"><a style="color: white; text-decoration: none;" href="#">Transaction Policy</a></li>
            </ul>
        </div>
        <div class="container text-center">
            <div class="col-md-12 pb-2">
                Copyright 2020 © COCO & JAY FASHION
            </div>
        </div>
    </footer>

</body>

</html>