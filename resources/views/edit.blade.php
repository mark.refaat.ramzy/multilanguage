@extends('layouts.app')
@section('content')
<div class="container">
    <form method="POST" action="{{ route('products.update', [ App::getLocale(), $product]) }}">
        @csrf
        @method("PUT")
        <div class="form-group">
            <label for="title">{{ __('products_table.title') }}</label>
            <input type="text" class="form-control" placeholder="" id="title" name="title"
                value="{{ $product->translation[0]->title }}">
        </div>
        <br>
        <div class="form-group">
            <label for="description">{{ __('products_table.desc') }}</label>
            <input type="text" class="form-control" placeholder="" id="description" name="description"
                value="{{ $product->translation[0]->description }}">
        </div>
        <br>
        <div class="form-group">
            <label for="price">{{ __('products_table.price') }}</label>
            <input type="number" class="form-control" placeholder="" id="price" name="price"
                value="{{ $product->price }}">
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection