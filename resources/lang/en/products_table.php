<?php

return [
    'edit' => 'edit',
    'delete' => 'delete',
    'title' => "Title",
    'desc' => "Description",
    'price' => "Price",
    'add' => "Add",
];
