<?php

return [
    "app_name" => 'multiLang',
    "dir" => "ltr",
    "navbar_products_link" => "products",
    "delete_product_confirm_message" => "Are you sure ?",
];
