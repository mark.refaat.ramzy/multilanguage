<?php

return [
    "app_name" => 'لغات متعددة',
    "dir" => "rtl",
    "navbar_products_link" => "الاصناف",
    "delete_product_confirm_message" => "تاكيد ؟",
];
