<?php

return [
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'title' => "الاسم",
    'desc' => "الوصف",
    'price' => "السعر",
    'add' => "اضافة",
];
