<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', App::getLocale());
Route::group(['prefix' => '{locale}', 'middleware' => 'setlocale'], function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('home');
    Route::resource('products', 'ProductController');
    Route::get('basket', function () {
        return view('basket');
    })->name('basket');
    Route::get('checkout', function () {
        return view('checkout');
    })->name('checkout');
    Route::get('addToCart/{id}/{name}/{price}', function ($l, $id, $name, $price) {
        // Session::flush();
        if (!Session::has('cart')) {
            Session::put('cart', []);
        }
        $totalBasket = floatval(Session::get('totalBasket'));
        Session::put('totalBasket', $totalBasket + $price);
        Session::push('cart', [$id, $name, $price]);
        Session::save();
        // dd(Session::all());
        return redirect()->back();
    })->name('addToCart');
});
