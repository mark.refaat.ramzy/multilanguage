<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    function product()
    {
        return $this->belongsTo('App\Product');
    }
}
