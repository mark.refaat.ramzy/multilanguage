<?php

namespace App\Http\Controllers;

use App\Language;
use App\Product;
use App\ProductTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::whereHas('translation', function ($query) {
            $query->where('language_code', App::getLocale());
        })->with(['translation' => function ($query) {
            $query->where('language_code',  App::getLocale());
        }])->get();
        return view('allProducts', compact('products'));
    }

    public function create()
    {
        return view('create');
    }

    public function store($locale, Request $request)
    {
        $product = new Product();
        $product->price = $request->price;
        $product->save();
        // First Idea
        $product->translation[0] = new ProductTranslation();
        $product->translation[0]->product_id = $product->id;
        $product->translation[0]->language_code = App::getLocale();
        $product->translation[0]->title = $request->titleEn;
        $product->translation[0]->description = $request->descriptionEn;
        $product->translation[0]->save();

        // Second Idea
        // $languages = Language::all();
        // foreach ($languages as $i => $language) {
        //     $product->translation[$i] = new ProductTranslation();
        //     $product->translation[$i]->product_id = $product->id;
        //     $product->translation[$i]->language_code = $language->code;
        //     $product->translation[$i]->title = $request->title;
        //     $product->translation[$i]->description = $request->description;
        //     $product->translation[$i]->save();
        // }
        return redirect(route('products.index', [App::getLocale()]));
    }

    public function edit($locale, $id)
    {
        $product = Product::with(['translation' => function ($query) {
            $query->where('language_code',  App::getLocale());
        }])->where('id', $id)->first();
        return view('edit', compact('product'));
    }

    public function update($locale, $id, Request $request)
    {
        // Product::where('id', $id);
        $product = Product::where('id', $id)->with(['translation' => function ($query) {
            $query->where('language_code',  App::getLocale());
        }])->first();
        $product->price = $request->price;
        $product->translation[0]->title = $request->title;
        $product->translation[0]->description = $request->description;
        $product->translation[0]->save();
        $product->save();
        return redirect(route('products.index', [App::getLocale()]));
    }

    public function destroy($locale, $id)
    {
        Product::where('id', $id)->delete();
        return redirect()->back();
    }
}
